//
//  MyoApp.cpp
//  MyoWekInput
//
//  Created by jash on 18/04/16.
//

#include "MyoApp.h"

void MyoApp::sendOsc() {
    
    const float emg0 = mEmg[0];
    const float emg1 = mEmg[1];
    const float emg2 = mEmg[2];
    const float emg3 = mEmg[3];
    const float emg4 = mEmg[4];
    const float emg5 = mEmg[5];
    const float emg6 = mEmg[6];
    const float emg7 = mEmg[7];
    
    const float quatX = mQuaternion[0];
    const float quatY = mQuaternion[1];
    const float quatZ = mQuaternion[2];
    const float quatW = mQuaternion[3];
    
    const float accelX = mAccelleration[0];
    const float accelY = mAccelleration[1];
    const float accelZ = mAccelleration[2];
    
    const float gyroX = mGyroscope[0];
    const float gyroY = mGyroscope[1];
    const float gyroZ = mGyroscope[2];
    
    
    osc::OutboundPacketStream p(buffer, OUTPUT_BUFFER_SIZE);
    p <<
    osc::BeginMessage("/wek/inputs") <<
    emg0 << emg1 << emg2 << emg3 << emg4 << emg5 << emg6 << emg7 <<
    quatX << quatY << quatZ << quatW <<
    accelX << accelY << accelZ <<
    gyroX << gyroY << gyroZ <<
    osc::EndMessage;
    
    mTransmitSocket->Send(p.Data(), p.Size());
    
}

void MyoApp::draw() {
    using std::atan2;
    using std::asin;
    using std::sqrt;
    
    const int offsetX = 3;
    const float scale = 23;
    const float emgScale = 80-4-offsetX;


    // Gyroscope
    // assume the max deg rotation per second is not more than 45 deg
    const int p_g_x_scaled = fmin( scale, std::abs( mGyroscope[0]*scale/45.0 ) );
    const int p_g_y_scaled = fmin( scale, std::abs( mGyroscope[1]*scale/45.0 ) );
    const int p_g_z_scaled = fmin( scale, std::abs( mGyroscope[2]*scale/45.0 ) );
    
    // Calculate Euler angles (roll, pitch, and yaw) from the unit quaternion.
    const float x = mQuaternion[0];
    const float y = mQuaternion[1];
    const float z = mQuaternion[2];
    const float w = mQuaternion[3];
    
    // Euler angles (local)
    const float roll = atan2(2.0f * (w * x + y * z), 1.0f - 2.0f * (x * x + y * y));
    const float pitch = asin(2.0f * (w * y - z * x) );
    const float yaw = atan2(2.0f * (w * z + x * y), 1.0f - 2.0f * (y * y + z * z));
    
    // roll, pitch, yaw in values between 0 -> scale asin and atan2 map to values betwen -M_PI to +M_PI
    const int rollScaled = fmin(scale,((roll/M_PI) + 1.0)*scale*0.5 );
    const int pitchScaled = fmin(scale,((pitch/M_PI) + 1.0 )*scale*0.5 );
    const int yawScaled = fmin(scale,((yaw/M_PI) + 1.0 )*scale*0.5 );
    
    // Accelerometer
    const int accelXScaled = fmin(scale,fabs(0.5f*scale+mAccelleration[0]*scale*0.25f)); // half is 0 g
    const int accelYScaled = fmin(scale,fabs(0.5f*scale+mAccelleration[1]*scale*0.25f));
    const int accelZScaled = fmin(scale,fabs(0.5f*scale+mAccelleration[2]*scale*0.25f));
    
    
    const int emg0Scaled = fmin( emgScale, mEmg[0]*emgScale );
    const int emg1Scaled = fmin( emgScale, mEmg[1]*emgScale );
    const int emg2Scaled = fmin( emgScale, mEmg[2]*emgScale );
    const int emg3Scaled = fmin( emgScale, mEmg[3]*emgScale );
    const int emg4Scaled = fmin( emgScale, mEmg[4]*emgScale );
    const int emg5Scaled = fmin( emgScale, mEmg[5]*emgScale );
    const int emg6Scaled = fmin( emgScale, mEmg[6]*emgScale );
    const int emg7Scaled = fmin( emgScale, mEmg[7]*emgScale );
    
    int curX = 0;
    int curY = 1; // give it some space
    
    mvwprintw( window, curY++,0,"Roll/Pitch/Yaw:");
    
    
    curX = offsetX;
    
    const int RPY[] = {rollScaled,pitchScaled,yawScaled};
    
    for( int vi=0; vi<3; vi++ ) {
        const int v = RPY[vi];

        mvwaddch( window, curY,curX,'[');
        
        if( v > 1 ) {
            for( int i=0; i<v-1; i++ ) {
                curX++;
                mvwaddch( window, curY,curX,' ');
            }
            curX++;
            mvwaddch( window, curY,curX,'*');

        } else {
            for( int i=0; i<v; i++ ) {
                curX++;
                mvwaddch( window, curY,curX,' ');
            }
        }
        
        for( int i=v; i<(int)scale; i++ ) {
            curX++;
            mvwaddch( window, curY,curX,' ');
        }
        
        curX++;
        mvwaddch( window, curY,curX,']');
        
        curX++;
        
        if( vi<2 ) {
            curX++;
            mvwaddch( window, curY,curX,' ');
        }
    }
    
    curY+=2;
    curX = offsetX;
    
    mvwprintw( window, curY++,0,"Accelerometer (X/Y/Z):");
    
    const int ACCEL[] = {accelXScaled,accelYScaled,accelZScaled};
    
    for( int vi=0; vi<3; vi++ ) {
        const int v = ACCEL[vi];
        mvwaddch( window, curY,curX,'[');
        
        
        if( v > 1 ) {
            for( int i=0; i<v-1; i++ ) {
                curX++;
                mvwaddch( window, curY,curX,' ');
            }
            
            curX++;
            mvwaddch( window, curY,curX,'*');
        } else {
            for( int i=0; i<v; i++ ) {
                curX++;
                mvwaddch( window, curY,curX,' ');
            }
        }
        
        for( int i=v; i<(int)scale; i++ ) {
            curX++;
            mvwaddch( window, curY,curX,' ');
        }
        
        curX++;
        mvwaddch( window, curY,curX,']');
        
        curX++;
        
        if( vi<2 ) {
            curX++;
            mvwaddch( window, curY,curX,' ');
        }
    }
    
    curY+=2;
    curX = offsetX;
    
    mvwprintw( window, curY++,0,"Gyroscope (X/Y/Z):");
    
    const int GYRO[] = {p_g_x_scaled,p_g_y_scaled,p_g_z_scaled};
    
    for( int vi=0; vi<3; vi++ ) {
        const int v = GYRO[vi];
        mvwaddch( window, curY,curX,'[');
        
        if( v > 1 ) {
            for( int i=0; i<v; i++ ) {
                curX++;
                mvwaddch( window, curY,curX,'#');
            }
            
        } else {
            for( int i=0; i<v; i++ ) {
                curX++;
                mvwaddch( window, curY,curX,' ');
            }
        }
        
        for( int i=v; i<(int)scale; i++ ) {
            curX++;
            mvwaddch( window, curY,curX,' ');
        }
        
        curX++;
        mvwaddch( window, curY,curX,']');
        
        curX++;
        
        if( vi<2 ) {
            curX++;
            mvwaddch( window, curY,curX,' ');
        }
    }
    
    curY+=2;
    curX = offsetX;
    
    mvwprintw( window, curY++,0,"EMG (Sensors 0-7):");
    
    const int EMG[] = {emg0Scaled,emg1Scaled,emg2Scaled,emg3Scaled,emg4Scaled,emg5Scaled,emg6Scaled,emg7Scaled};
    
    for( int vi=0; vi<8; vi++ ) {
        const int v = EMG[vi];
        
        mvwaddch( window, curY,curX++, 48+vi);
        mvwaddch( window, curY,curX++, ' ');
        mvwaddch( window, curY,curX,'[');
        
        if( v>1 ) {
            for( int i=0; i<v; i++ ) {
                curX++;
                mvwaddch( window, curY,curX,'#');
            }
        } else {
            for( int i=0; i<v; i++ ) {
                curX++;
                mvwaddch( window, curY,curX,' ');
            }
        }

        for( int i=v; i<(int)emgScale; i++ ) {
            curX++;
            mvwaddch( window, curY,curX,' ');
        }
        
        curX++;
        mvwaddch( window, curY,curX,']');
        
        curX = offsetX;
        curY++;
        /*
        curX++;
        
        if( vi<7 ) {
            curX++;
            mvwaddch( window, curY,curX,' ');
        }
         */
    }
    
    curY++;
    curX = offsetX;
    
    mvwprintw( window, curY++,0,"Sending 18 values:");
    mvwprintw( window, curY++,offsetX," 0- 7: EMG");
    mvwprintw( window, curY++,offsetX," 8-11: Rotation Quaternion");
    mvwprintw( window, curY++,offsetX,"12-14: Acceleration");
    mvwprintw( window, curY++,offsetX,"15-17: Gyroscope");
    
    refresh();
    
    /*
     if (onArm) {
     // Print out the currently recognized pose and which arm Myo is being worn on.
     
     // Pose::toString() provides the human-readable name of a pose. We can also output a Pose directly to an
     // output stream (e.g. std::cout << currentPose;). In this case we want to get the pose name's length so
     // that we can fill the rest of the field with spaces below, so we obtain it as a string using toString().
     std::string poseString = currentPose.toString();
     
     std::cout << '[' << (whichArm == myo::armLeft ? "L" : "R") << ']'
     << '[' << poseString << std::string(14 - poseString.size(), ' ') << ']';
     } else {
     // Print out a placeholder for the arm and pose when Myo doesn't currently know which arm it's on.
     std::cout << "[?]" << '[' << std::string(14, ' ') << ']';
     }
     */
    
}

void MyoApp::resetSensorData() {
    std::fill_n(mEmg, 8, 0.0f);
    std::fill_n(mAccelleration, 3, 0.0f);
    std::fill_n(mGyroscope, 3, 0.0f);
    std::fill_n(mQuaternion, 4, 0 );
}

MyoApp::MyoApp() : MyoApp( "127.0.0.1", 6448 ) {}
MyoApp::MyoApp(const char *host) : MyoApp( host, 6448 ) {}
MyoApp::MyoApp(int port) : MyoApp( "127.0.0.1", port ) {}

MyoApp::MyoApp(const char *host, int port) {
    mTransmitSocket = new UdpTransmitSocket(IpEndpointName(host, port));
    resetSensorData();
    window = initscr();
    nocbreak();
    noecho();
    curs_set(0);
}

MyoApp::~MyoApp() {
    delwin(window);
    delete mTransmitSocket;
}

// units of g
void MyoApp::onAccelerometerData(myo::Myo* myo, uint64_t timestamp, const myo::Vector3<float>& accel)
{
    // g
    mAccelleration[0] = accel.x();
    mAccelleration[1] = accel.y();
    mAccelleration[2] = accel.z();
}

// units of deg/s
void MyoApp::onGyroscopeData(myo::Myo* myo, uint64_t timestamp, const myo::Vector3<float>& gyro)
{
    // values are deg per second
    mGyroscope[0] = gyro.x();
    mGyroscope[1] = gyro.y();
    mGyroscope[2] = gyro.z();
}

// onOrientationData() is called whenever the Myo device provides its current orientation, which is represented
// as a unit quaternion.
void MyoApp::onOrientationData(myo::Myo* myo, uint64_t timestamp, const myo::Quaternion<float>& quat)
{
    // values should be between 0 and 1
    mQuaternion[0] = quat.x();
    mQuaternion[1] = quat.y();
    mQuaternion[2] = quat.z();
    mQuaternion[3] = quat.w();
}
/*
 // onPose() is called whenever the Myo detects that the person wearing it has changed their pose, for example,
 // making a fist, or not making a fist anymore.
 void onPose(myo::Myo* myo, uint64_t timestamp, myo::Pose pose)
 {
 currentPose = pose;
 
 osc::OutboundPacketStream p(buffer, OUTPUT_BUFFER_SIZE);
 p << osc::BeginMessage("/myo/pose")
 << MAC
 << currentPose.toString().c_str() << osc::EndMessage;
 transmitSocket->Send(p.Data(), p.Size());
 
 // Vibrate the Myo whenever we've detected that the user has made a fist.
 if (pose == myo::Pose::fist) {
 myo->vibrate(myo::Myo::vibrationShort);
 }
 }
 */
void MyoApp::onEmgData(myo::Myo* myo, uint64_t timestamp, const int8_t* emg) {
    osc::OutboundPacketStream p(buffer, OUTPUT_BUFFER_SIZE);
    
    // values between 0.0 and 1.0
    mEmg[0] = (((float)(emg[0])/(float)0x80) + 1.0)*0.5;
    mEmg[1] = (((float)(emg[1])/(float)0x80) + 1.0)*0.5;
    mEmg[2] = (((float)(emg[2])/(float)0x80) + 1.0)*0.5;
    mEmg[3] = (((float)(emg[3])/(float)0x80) + 1.0)*0.5;
    mEmg[4] = (((float)(emg[4])/(float)0x80) + 1.0)*0.5;
    mEmg[5] = (((float)(emg[5])/(float)0x80) + 1.0)*0.5;
    mEmg[6] = (((float)(emg[6])/(float)0x80) + 1.0)*0.5;
    mEmg[7] = (((float)(emg[7])/(float)0x80) + 1.0)*0.5;
}

/*
 // onArmRecognized() is called whenever Myo has recognized a setup gesture after someone has put it on their
 // arm. This lets Myo know which arm it's on and which way it's facing.
 void onArmRecognized(myo::Myo* myo, uint64_t timestamp, myo::Arm arm, myo::XDirection xDirection)
 {
 onArm = true;
 whichArm = arm;
 
 osc::OutboundPacketStream p(buffer, OUTPUT_BUFFER_SIZE);
 p << osc::BeginMessage("/myo/onarm")
 << MAC
 << (whichArm == myo::armLeft ? "L" : "R") << osc::EndMessage;
 transmitSocket->Send(p.Data(), p.Size());
 }
 
 // onArmLost() is called whenever Myo has detected that it was moved from a stable position on a person's arm after
 // it recognized the arm. Typically this happens when someone takes Myo off of their arm, but it can also happen
 // when Myo is moved around on the arm.
 void onArmLost(myo::Myo* myo, uint64_t timestamp)
 {
 onArm = false;
 osc::OutboundPacketStream p(buffer, OUTPUT_BUFFER_SIZE);
 p << osc::BeginMessage("/myo/onarmlost")
 << MAC
 << osc::EndMessage;
 transmitSocket->Send(p.Data(), p.Size());
 }
 
 // There are other virtual functions in DeviceListener that we could override here, like onAccelerometerData().
 // For this example, the functions overridden above are sufficient.
 
 // We define this function to print the current values that were updated by the on...() functions above.
 */

void MyoApp::update() {
    // send OSC
    draw();
    sendOsc();
}


