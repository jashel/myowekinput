//
//  main.cpp
//  MyoWekInput
//
//  Created by jash on 18/04/16.
//

#include <stdio.h>
// stop oscpack sprintf warnings
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

//#define MAC myo->macAddressAsString().c_str()
#define MAC "00:00:00:00:00:00" // beta release 1 removed macs :(

#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <iomanip>
#include <stdexcept>
#include <string>

#include "MyoApp.h"

int main(int argc, char** argv)
{
    // We catch any exceptions that might occur below -- see the catch statement for more details.
    try
    {
        MyoApp* app;

        if (argc != 3 && argc != 2 && argc != 1)
        {
            std::cout << "\nusage: " << argv[0] << " [IP address] <port>\n\n" <<
            "Myo-OSC sends OSC output over UDP from the input of a Thalmic Myo armband.\n" <<
            "IP address defaults to 127.0.0.1/localhost\n\n" <<
            "by Samy Kamkar -- http://samy.pl -- code@samy.pl\n";
            exit(0);
        }
        
        if (argc == 1)
        {
            app = new MyoApp();
        }
        else if (argc == 2)
        {
            app = new MyoApp(atoi(argv[1]));
        }
        else if (argc == 3)
        {
            app = new MyoApp(argv[1], atoi(argv[2]));
        }
        else
        {
            std::cout << "well this awkward -- weird argc: " << argc << "\n";
            exit(0);
        }
        
        
        // First, we create a Hub with our application identifier. Be sure not to use the com.example namespace when
        // publishing your application. The Hub provides access to one or more Myos.
        myo::Hub hub("com.samy.myo-osc");
        
        std::cout << "Attempting to find a Myo..." << std::endl;
        
        // Next, we attempt to find a Myo to use. If a Myo is already paired in Myo Connect, this will return that Myo
        // immediately.
        // waitForAnyMyo() takes a timeout value in milliseconds. In this case we will try to find a Myo for 10 seconds, and
        // if that fails, the function will return a null pointer.
        myo::Myo* myo = hub.waitForMyo(10000);
        
        // If waitForAnyMyo() returned a null pointer, we failed to find a Myo, so exit with an error message.
        if (!myo) {
            throw std::runtime_error("Unable to find a Myo!");
        }
        
        // We've found a Myo.
        std::cout << "Connected to a Myo armband!" << std::endl << std::endl;
        
        myo->setStreamEmg(myo::Myo::streamEmgEnabled);
        
        // Next we construct an instance of our DeviceListener, so that we can register it with the Hub.
        
        // Hub::addListener() takes the address of any object whose class inherits from DeviceListener, and will cause
        // Hub::run() to send events to all registered device listeners.
        hub.addListener(app);
        
        // Finally we enter our main loop.
        while (1) {
            // In each iteration of our main loop, we run the Myo event loop for a set number of milliseconds.
            // In this case, we wish to update our display 20 times a second, so we run for 1000/20 milliseconds.
            hub.run(1000/30);
            // After processing events, we call the print() member function we defined above to print out the values we've
            // obtained from any events that have occurred.
            //collector.print();
            app->update();
        }
        
        // If a standard exception occurred, we print out its message and exit.
    } catch (const std::exception& e) {
        std::cerr << "Error: " << e.what() << std::endl;
        std::cerr << "Press enter to continue.";
        std::cin.ignore();
        return 1;
    }
}
