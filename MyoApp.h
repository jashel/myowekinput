//
//  MyoApp.h
//  MyoWekInput
//
//  Created by jash on 18/04/16.
//

#ifndef MyoApp_h
#define MyoApp_h

// The only file that needs to be included to use the Myo C++ SDK is myo.hpp.
#include <myo/myo.hpp>

// add oscpack
#include "osc/OscOutboundPacketStream.h"
#include "ip/UdpSocket.h"
#include "ncurses.h"

#define OUTPUT_BUFFER_SIZE 1024

class MyoApp : public myo::DeviceListener {

public:
    MyoApp( const char *host, int port );
    MyoApp( int port );
    MyoApp( const char *host );
    MyoApp( );

    ~MyoApp();
    void onAccelerometerData(myo::Myo* myo, uint64_t timestamp, const myo::Vector3<float>& accel) override;
    void onGyroscopeData(myo::Myo* myo, uint64_t timestamp, const myo::Vector3<float>& gyro) override;
    void onOrientationData(myo::Myo* myo, uint64_t timestamp, const myo::Quaternion<float>& quat) override;
    void onEmgData(myo::Myo* myo, uint64_t timestamp, const int8_t* emg) override;
    void update();
    
private:
    
    WINDOW* window;
    
    UdpTransmitSocket* mTransmitSocket;

    
    float mEmg[8];
    float mAccelleration[3];
    float mGyroscope[3];
    float mQuaternion[4];

    char buffer[OUTPUT_BUFFER_SIZE];
    
    void sendOsc();
    void draw();
    
    void resetSensorData();
};



#endif /* MyoApp_h */
